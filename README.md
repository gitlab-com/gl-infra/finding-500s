This repository is a script that looks for recent public facing 500s on
GitLab.com

## Usage

* Set the elastic search password in your environment

```
export ELASTIC_PASSWORD=*****
```

* Run the script
```
./find-500s
```
